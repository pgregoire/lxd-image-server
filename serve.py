#!/usr/bin/env python
#
# LXD images server
#
# Copyright 2019, Philippe Gregoire <pg@pgregoire.xyz>
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#

import SocketServer
import SimpleHTTPServer
import getopt
import hashlib
import urllib
import sys


def sha256(f):
    with open(f, 'rb') as fp:
        return hashlib.sha256(fp.read()).hexdigest().encode('ascii')



class Proxy(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def path_is_valid(self):
        return '..' not in self.path and '/' == self.path[0]


    def get_host(self):
        for h in str(self.headers).split('\r\n'):
            s = h.split(':')
            if 'host' == s[0].strip().lower():
                return ':'.join(s[1:]).strip()
        

    def custom_headers(self):
        self.send_header('LXD-Image-Hash', sha256(self.path[1:]))
        self.send_header('LXD-Image-URL', 'https://{}{}'.format(self.get_host(), self.path))
        self.end_headers()


    def do_GET(self):
        if self.path_is_valid():
            self.send_response(200 if self.get_host() else 500)
            self.custom_headers()
            self.copyfile(urllib.urlopen(self.path[1:]), self.wfile)
        else:
            self.send_response(404)


    def do_HEAD(self):
        if self.path_is_valid():
            self.send_response(200)
            self.custom_headers()
        else:
            self.send_response(404)


def main(args):
    def usage(fp=sys.stdout):
        u = '{} [-h] [port]'.format(sys.argv[0])
        fp.write('usage: {}\n'.format(u))
        fp.flush()


    try:
        opts, args = getopt.getopt(sys.argv[1:], 'h')
    except getopt.GetoptError as e:
        sys.stderr.write('{}\n'.format(e))
        usage(fp=sys.stderr)
        return 2

    for k, v in opts:
        if '-h' == k:
            usage()
            return 0

    if len(args) not in [0, 1]:
        usage(fp=sys.stderr)
        return 2

    o_port = 8080 if 0 == len(args) else int(args[0])
    SocketServer.ForkingTCPServer.allow_reuse_address = True
    SocketServer.ForkingTCPServer(('127.0.0.1', o_port), Proxy).serve_forever()


if '__main__' == __name__:
    sys.exit(main(sys.argv[1:]))
