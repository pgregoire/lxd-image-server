# lxd-image-server

A simple script to server `.tar.gz` files in the current directory
as LXD images over HTTP. This script binds to `127.0.0.1` (port `8080`,
by default). It is up to the service operator to configure a reverse
proxy or configure the packet filter appropriately.

## Usage

```
cd /path/to/images && python /path/to/script/serve.py
```

## Limitations

The architecture indicator sent by the client is not honored. Merge
requests are welcomed!
